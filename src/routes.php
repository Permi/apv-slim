<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include 'helper/dbHelper.php';

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


// Vypis osob
$app->get('/persons', function (Request $request, Response $response, $args) {
    $tplVars['message'] = empty($request->getQueryParams()[base64_encode('message')]) ? null : base64_decode($request->getQueryParams()[base64_encode('message')]);
    //ulozeny vysledek query jako objekt
    $stmt = $this->db->query('SELECT * FROM person ORDER BY first_name');
    // Je treba prevest na datovou strukturu jazyka php
    $tplVars['osoby'] = $stmt->fetchAll();
    /* obsah promenne tplVars
     * [
     *  [first_name => "Pepa", last_name => "Novak", height => 156],
     *  [first_name => "Jan", last_name => "Blatny", height => 153]
     * ]
     */
    $stmt = $this->db->query('SELECT first_name, last_name, nickname FROM person ORDER BY first_name');
    $tplVars['persons'] = $stmt->fetchAll();


    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');


$app->post('/persons', function (Request $request, Response $response, $args) {
    $search_preprocess = $request->getParsedBody()['search'];
    $search = '';
    for ($i = 0; $i <= strlen($search_preprocess); $i++) {
        if (substr($search_preprocess, $i, 1) == ';') {
            $search .= '\\';
        }
        $search .= substr($search_preprocess, $i, 1);
    }
    // Not possible to bind value for like statement, so at least minimal protection...
    $search = "'%".$search."%'";
    $stmt = $this->db->query("SELECT * FROM person
                                WHERE first_name LIKE $search OR last_name LIKE $search OR nickname LIKE $search
                                      ORDER BY first_name");
    $tplVars['osoby'] = $stmt->fetchAll();

    $stmt = $this->db->query('SELECT first_name, last_name, nickname FROM person ORDER BY first_name');
    $tplVars['persons'] = $stmt->fetchAll();
    return $this->view->render($response, 'persons.latte', $tplVars);
});


/* Formular pro pridani osoby */
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
        'contact' => '',
        'contact_type' => ''
    ];
    return $this->view->render($response, 'newPerson.latte', $tplVars);
})->setName('newPerson');


$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) ||
            !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender)
                                            VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");
            $stmt->bindValue(":last_name", $formData['last_name']); // ;DROP TABLE person; ===> \;DROP TABLE person\;
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":nickname", $formData['nickname']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":birth_day", empty($formData['birth_day']) ? null : $formData['birth_day']);
            $stmt->bindValue(":height", empty($formData['height']) ? null : $formData['height']);
            $stmt->bindValue(":gender", empty($formData['gender']) ? null : $formData['gender']);
            $stmt->execute();
            $tplVars['message'] = 'Person successfuly added';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong. Try again';
            if ($e->getCode() == 23505) {
                $tplVars['message'] = 'Person already exist';
            }
            $this->logger->error($e->getMessage());
            $tplVars['formData'] = $formData;
            return $this->view->render($response->withStatus(409), 'newPerson.latte', $tplVars);
        }
        if (!empty($formData['contact'])) {
            if ($formData['contact_type'] == '') {
                $tplVars['message'] = 'Choose contact Type';
            } else {
                $id = $this->db->lastInsertId('person_id_person_seq');
                insert_contact($this->db, $formData, $id, $this->logger);
                $contacts = $this->db->prepare("SELECT * FROM contact JOIN contact_type USING (id_contact_type) 
                            WHERE id_person = $id ORDER BY id_contact");
                $contacts->execute();
                $tplVars['contacts'] = $contacts->fetchAll();
            }
        }
    }
    $stmt = $this->db->prepare('SELECT id_person FROM person 
                                WHERE nickname = :nickname AND first_name = :first_name AND last_name = :last_name');
    $stmt->bindValue(":nickname", $formData['nickname']);
    $stmt->bindValue(":first_name", $formData['first_name']);
    $stmt->bindValue(":last_name", $formData['last_name']);
    $stmt->execute();
    $personId = $stmt->fetch()['id_person'];
    $tplVars['formData'] = $formData;
    return $response->withRedirect($this->router->pathFor('personsUpdate') . '?id_person=' . $personId);
});


$app->get('/persons/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $personId = $params['id_person'];
    if (empty($personId)) {
        $tplVars['error'] = 'ID is missing';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    } else {
        $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) WHERE
                                    id_person = :id_person");
        $stmt->bindValue(":id_person", $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();

        $contacts = $this->db->prepare("SELECT * FROM contact JOIN contact_type USING (id_contact_type) 
                                WHERE id_person = :id_person ORDER BY id_contact");
        $contacts->bindValue(":id_person", $params['id_person']);
        $contacts->execute();
        $tplVars['contacts'] = $contacts->fetchAll();
    }
    if (empty($tplVars['formData'])) {
        $tplVars['error'] = 'Person not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }

    $tplVars['formData']['contact'] = '';
    $tplVars['formData']['contact_type'] = '';
    return $this->view->render($response, 'personsUpdate.latte', $tplVars);
})->setName('personsUpdate');


$app->post('/persons/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $params = $request->getQueryParams();
    $personId = $params['id_person'];
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        // Zapis location
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) ||
            !empty($formData['zip'])) {
            $stmt = $this->db->prepare("SELECT id_location FROM person WHERE id_person = :id_person");
            $stmt->bindValue(':id_person', $personId);
            $stmt->execute();
            $id_location = $stmt->fetch()['id_location'];

            if ($id_location == null) {
                $id_location = insert_location($this->db, $formData);
            } else {
                try {
                    $stmt = $this->db->prepare("UPDATE location SET city = :city, street_name = :street_name,
                                                street_number = :street_number, zip = :zip WHERE id_location = $id_location");
                    $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
                    $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
                    $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
                    $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
                    $stmt->execute();

                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
            }
        }

        // Zapis person
        try {
            $stmt = $this->db->prepare("UPDATE person SET nickname = :nickname, first_name = :first_name, 
                                            last_name = :last_name, id_location = :id_location, birth_day = :birth_day,
                                             height = :height, gender = :gender WHERE id_person = :id_person");
            $stmt->bindValue(":last_name", $formData['last_name']);
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":nickname", $formData['nickname']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":birth_day", empty($formData['birth_day']) ? null : $formData['birth_day']);
            $stmt->bindValue(":height", empty($formData['height']) ? null : $formData['height']);
            $stmt->bindValue(":gender", empty($formData['gender']) ? null : $formData['gender']);
            $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
            $stmt->execute();
            $tplVars['message'] = 'Person successfuly updated';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong.';
            $response = $response->withStatus(409);
            $this->logger->error($e->getMessage());
        }
        // Zapis contact
        $stmt = $this->db->prepare("SELECT * FROM contact WHERE id_person = :id_person ORDER BY (id_contact)");
        $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
        $stmt->execute(); // Ziskame id vsech kontaktu, abychom mohli pracovat s daty z formulare (neznamy name)
        $contactIds = $stmt->fetchAll();
        foreach ($contactIds as $c) {
            $id = $c['id_contact'];
            try {
                $stmt = $this->db->prepare("UPDATE contact SET id_contact_type = :id_contact_type, contact = :contact
                                            WHERE id_contact = $id");
                $stmt->bindValue(":id_contact_type", $formData['contact_type_'.$id]);
                $stmt->bindValue(":contact", $formData['contact_'.$id]);
                $stmt->execute();
            } catch (PDOException $e) {
                echo $e->getMessage();
                $formData['message'] = 'Contact save was unsuccessful';
            }
        }

        if (!empty($formData['contact'])) {
            if (empty($formData['contact_type'])) {
                $tplVars['message'] = 'Choose contact Type';
            } else {
                insert_contact($this->db, $formData, $personId, $this->logger);
                $formData['contact'] = ''; // Vynulujeme, aby bylo mozne zadat novou hodnotu
                $formData['contact_type'] = '';

            }
        }
        // Aktualizujeme polozku contacts;
        $stmt = $this->db->prepare("SELECT * FROM contact JOIN contact_type USING (id_contact_type)
                                    WHERE id_person = :id_person ORDER BY (id_contact)");
        $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
        $stmt->execute(); // Nacteme aktualni hodnoty
        $tplVars['contacts'] = $stmt->fetchAll();
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'personsUpdate.latte', $tplVars);
});


$app->get('/persons/info', function (Request $request, Response $response, $args) {
    $personId = $request->getQueryParams()['id_person'];

    $stmt = $this->db->prepare("SELECT * FROM person WHERE id_person = :id_person");
    $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
    $stmt->execute();
    $tplVars['person'] = $stmt->fetch();

    if (empty($tplVars['person'])) {
        $tplVars['error'] = 'Person not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }

    $stmt = $this->db->prepare("SELECT * FROM location WHERE id_location = :id_location");
    $stmt->bindValue(":id_location", $tplVars['person']['id_location']);
    $stmt->execute();
    $tplVars['location'] = $stmt->fetch();

    $stmt = $this->db->prepare("SELECT * FROM contact LEFT JOIN contact_type USING (id_contact_type)
                                WHERE id_person = :id_person ORDER BY (id_contact)");
    $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
    $stmt->execute();
    $tplVars['contact'] = $stmt->fetchAll();

    $stmt = $this->db->prepare("SELECT * FROM person_meeting LEFT JOIN meeting USING (id_meeting) 
                                WHERE id_person = :id_person ORDER BY (id_meeting)");
    $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
    $stmt->execute();
    $tplVars['meeting'] = $stmt->fetchAll();

    $stmt = $this->db->prepare("SELECT * FROM relation LEFT JOIN relation_type USING (id_relation_type)
                                WHERE id_person1 = :id_person OR id_person2 = :id_person ORDER BY (id_relation)");
    $stmt->bindValue(":id_person", empty($personId) ? null : $personId);
    $stmt->execute();
    $tplVars['relation'] = $stmt->fetchAll();

    if (!empty($tplVars['relation'])) {
        $ids = array();
        foreach ($tplVars['relation'] as $r) {
            array_push($ids, (int)$r['id_person1'], (int)$r['id_person2']); //prepare all IDs of people with relationship for querying
        }
        $ids = array_unique($ids);

        foreach ($ids as $id) {
            $stmt = $this->db->prepare("SELECT id_person, first_name, last_name FROM person WHERE id_person = :id");
            $stmt->bindValue(":id", $id);
            $stmt->execute();
            $name = $stmt->fetch();
            $tplVars['name'][$name['id_person']] = $name['first_name'] . ' ' . $name['last_name'];
        }
    }


    return $this->view->render($response, 'personInfo.latte', $tplVars);
})->setName('personsInfo');


$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $personId = $request->getQueryParams()['id_person'];
    if (empty($personId)) {
        $message = 'Invalid ID, person could not be removed.';
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
            $stmt->bindValue(':id_person', $personId);
            $stmt->execute();
            $message = 'Person successfuly removed.';

        } catch (PDOException $e) {
            $this->logger->info($e);
            $message = 'Sorry, something went wrong while deleting person.';
        }
    }

    $tplVars[base64_encode('message')] = base64_encode($message);
    return $response->withRedirect($this->router->pathFor('persons', [], $tplVars));
})->setName('personsDelete');


/*------------------ Meetings -------------------*/

$app->get('/meetings', function (Request $request, Response $response, $args) {
    $tplVars['message'] = empty($request->getQueryParams()[base64_encode('message')]) ? null : base64_decode($request->getQueryParams()[base64_encode('message')]);

    $stmt = $this->db->query('SELECT *, 
                                    (SELECT count(*) as participants_count 
                                    FROM person_meeting WHERE person_meeting.id_meeting = meeting.id_meeting)
                                FROM meeting LEFT JOIN location USING (id_location) ORDER BY start');
    $tplVars['meetings'] = $stmt->fetchAll();

    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');


$app->get('/meetings/info', function (Request $request, Response $response, $args) {
    $meetingId = $request->getQueryParams()['id_meeting'];

    $stmt = $this->db->prepare("SELECT * FROM meeting
                                LEFT JOIN location USING (id_location)
                                WHERE id_meeting = :id_meeting");
    $stmt->bindValue(":id_meeting", empty($meetingId) ? null : $meetingId);
    $stmt->execute();
    $tplVars['meeting'] = $stmt->fetch();

    if (empty($tplVars['meeting'])) {
        $tplVars['error'] = 'Meeting not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }

    $stmt = $this->db->prepare("SELECT * FROM person_meeting
                                LEFT JOIN person USING (id_person)
                                WHERE id_meeting = :id_meeting ORDER BY first_name");
    $stmt->bindValue(":id_meeting", empty($meetingId) ? null : $meetingId);
    $stmt->execute();
    $tplVars['participant'] = $stmt->fetchAll();

    $stmt = $this->db->prepare("SELECT id_person, first_name, last_name 
                                FROM person WHERE id_person NOT IN
                                    (SELECT id_person FROM person_meeting
                                LEFT JOIN person USING (id_person)
                                WHERE id_meeting = :id_meeting)
                                ORDER BY first_name");
    $stmt->bindValue(":id_meeting", empty($meetingId) ? null : $meetingId);
    $stmt->execute();
    $tplVars['person'] = $stmt->fetchAll();
    $tplVars['id_person'] = '';

    return $this->view->render($response, 'meetingInfo.latte', $tplVars);
})->setName('meetingInfo');


$app->get('/meetings/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'start_date' => '',
        'start_time' => '',
        'description' => '',
        'duration' => '',
        'id_location' => null,
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
    ];
    return $this->view->render($response, 'newMeeting.latte', $tplVars);
})->setName('newMeeting');


$app->get('/meetings/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $meetingId = $params['id_meeting'];
    if (empty($meetingId)) {
        $tplVars['error'] = 'ID is missing';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    } else {
        $stmt = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location USING (id_location) WHERE
                                    id_meeting = :id_meeting");
        $stmt->bindValue(":id_meeting", $meetingId);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        $tplVars['formData']['start_date'] = substr($tplVars['formData']['start'], 0,10);
        $tplVars['formData']['start_time'] = substr($tplVars['formData']['start'], 11, 8);
    }

    if (empty($tplVars['formData'])) {
        $tplVars['error'] = 'Meeting not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }

    return $this->view->render($response, 'meetingsUpdate.latte', $tplVars);
})->setName('meetingsUpdate');


$app->post('/meetings/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['start_date']) or empty($formData['start_time']) or
    (empty($formData['city']) and empty($formData['street_name']) and empty($formData['street_number'])
     and empty($formData['zip']))) {
        $tplVars['message'] = 'Fill required fields and at least one field of location';
        $tplVars['formData'] = $formData;
        return $this->view->render($response->withStatus(409), 'newMeeting.latte', $tplVars);
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) ||
            !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $formData['start'] = $formData['start_date'] . ' ' . $formData['start_time'];
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location)
                                            VALUES (:start, :description, :duration, :id_location)");
            $stmt->bindValue(":start", $formData['start']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->execute();
            $tplVars['message'] = 'Meeting successfuly added';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong. Try again';
            if ($e->getCode() == 23505) {
                $tplVars['message'] = 'Meeting already exist';
            }
            $this->logger->error($e->getMessage());
            $tplVars['formData'] = $formData;
            return $this->view->render($response->withStatus(409), 'newMeeting.latte', $tplVars);
        }
    }

    $meetingId = $this->db->lastInsertId('meeting_id_meeting_seq');
    $tplVars['formData'] = $formData;
    return $response->withRedirect($this->router->pathFor('meetingsUpdate') . '?id_meeting=' . $meetingId);
});

$app->post('/meetings/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $params = $request->getQueryParams();
    $meetingId = $params['id_meeting'];
    if (empty($formData['start_date']) or empty($formData['start_time'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        // Zapis location
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) ||
            !empty($formData['zip'])) {
            $stmt = $this->db->prepare("SELECT id_location FROM person WHERE id_person = :id_meeting");
            $stmt->bindValue(':id_meeting', $meetingId);
            $stmt->execute();
            $id_location = $stmt->fetch()['id_location'];

            if ($id_location == null) {
                $id_location = insert_location($this->db, $formData);
            } else {
                try {
                    $stmt = $this->db->prepare("UPDATE location SET city = :city, street_name = :street_name,
                                                street_number = :street_number, zip = :zip WHERE id_location = $id_location");
                    $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
                    $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
                    $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
                    $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
                    $stmt->execute();

                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
            }
        }

        // Zapis meeting
        try {
            $formData['start'] = $formData['start_date'] . ' ' . $formData['start_time'];
            $stmt = $this->db->prepare("UPDATE meeting SET start = :start, description = :description, 
                                            duration = :duration, id_location = :id_location
                                            WHERE id_meeting = :id_meeting");
            $stmt->bindValue(":start", $formData['start']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":id_meeting", empty($meetingId) ? null : $meetingId);
            $stmt->execute();
            $tplVars['message'] = 'Meeting successfuly updated';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong.';
            $response = $response->withStatus(409);
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'meetingsUpdate.latte', $tplVars);
});

$app->post('/meetings/delete', function (Request $request, Response $response, $args) {
    $meetingId = $request->getQueryParams()['id_meeting'];
    if (empty($meetingId)) {
        $message = 'Invalid ID, meeting could not be removed.';
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM meeting WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':id_meeting', $meetingId);
            $stmt->execute();
            $message = 'Meeting successfuly removed.';

        } catch (PDOException $e) {
            $this->logger->info($e);
            $message = 'Sorry, something went wrong while deleting meeting.';
        }
    }

    $tplVars[base64_encode('message')] = base64_encode($message);
    return $response->withRedirect($this->router->pathFor('meetings', [], $tplVars));
})->setName('meetingsDelete');


$app->post('/meetings/delete-participant', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $meetingId = $params['id_meeting'];
    $personId = $params['id_person'];
    if (empty($meetingId) or empty($personId)) {
        $message = 'Invalid ID, participant could not be removed.';
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM person_meeting WHERE id_meeting = :id_meeting AND id_person = :id_person");
            $stmt->bindValue(':id_person', $personId);
            $stmt->bindValue(':id_meeting', $meetingId);
            $stmt->execute();
            $message = 'Participant successfuly removed.';

        } catch (PDOException $e) {
            $this->logger->info($e);
            $message = 'Sorry, something went wrong while deleting participant.';
        }
    }
    $tplVars['id_meeting'] = $meetingId;
    $tplVars[base64_encode('message')] = base64_encode($message);
    return $response->withRedirect($this->router->pathFor('meetingInfo', [], $tplVars));
})->setName('participantDelete');


$app->post('/meetings/add-participant', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $formData = $request->getParsedBody();
    $meetingId = $params['id_meeting'];
    $personId = $formData['id_person'];
    if (empty($meetingId) or empty($personId)) {
        $message = 'Invalid ID, participant could not be added.';
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)");
            $stmt->bindValue(':id_person', $personId);
            $stmt->bindValue(':id_meeting', $meetingId);
            $stmt->execute();
            $message = 'Participant successfuly added.';

        } catch (PDOException $e) {
            $this->logger->info($e);
            $message = 'Sorry, something went wrong while adding participant.';
        }
    }
    $tplVars['id_meeting'] = $meetingId;
    $tplVars[base64_encode('message')] = base64_encode($message);
    return $response->withRedirect($this->router->pathFor('meetingInfo', [], $tplVars));
})->setName('participantAdd');

/*------------------ Relations -------------------*/

$app->get('/relations', function (Request $request, Response $response, $args) {
    $tplVars['message'] = empty($request->getQueryParams()[base64_encode('message')]) ? null : base64_decode($request->getQueryParams()[base64_encode('message')]);

    $stmt = $this->db->query('SELECT * FROM relation 
                                LEFT JOIN (SELECT id_person as id_person1, first_name as first_name1, last_name as last_name1 FROM person) as person1 ON relation.id_person1 = person1.id_person1
                                LEFT JOIN (SELECT id_person as id_person2, first_name as first_name2, last_name as last_name2 FROM person) as person2 ON relation.id_person2 = person2.id_person2
                                LEFT JOIN relation_type rt on relation.id_relation_type = rt.id_relation_type
                                ORDER BY id_relation DESC');
    $tplVars['relations'] = $stmt->fetchAll();

    return $this->view->render($response, 'relations.latte', $tplVars);
})->setName('relations');


$app->get('/relations/new', function (Request $request, Response $response, $args) {
    $id_person = isset($request->getQueryParams()['id_person']) ? $request->getQueryParams()['id_person'] : '';
    $tplVars['formData'] = [
        'id_person1' => $id_person,
        'id_person2' => '',
        'description' => '',
        'id_relation_type' => null,
    ];
    $tplVars['id_person'] = $id_person;
    $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name"); //Variable deleted after iterating in template
    $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");
    return $this->view->render($response, 'newRelation.latte', $tplVars);
})->setName('newRelation');


$app->post('/relations/new', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParams()['id_person'];
    $formData = $request->getParsedBody();
    if (empty($formData['id_person1']) or empty($formData['id_person2']) or empty($formData['id_person1']))  {
        $tplVars['message'] = 'Fill required fields';
        $tplVars['formData'] = $formData;
        return $this->view->render($response->withStatus(409), 'newRelation.latte', $tplVars);
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO relation (id_person1, id_person2, description, id_relation_type)
                                            VALUES (:id_person1, :id_person2, :description, :id_relation_type)");
            $stmt->bindValue(":id_person1", $formData['id_person1']);
            $stmt->bindValue(":id_person2", $formData['id_person2']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":id_relation_type", empty($formData['id_relation_type']) ? null : $formData['id_relation_type']);
            $stmt->execute();
            $tplVars['message'] = 'Relation successfuly added';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong. Try again';
            if ($e->getCode() == 23505) {
                $tplVars['message'] = 'Relation already exist';
            }
            $this->logger->error($e->getMessage());
            $tplVars['formData'] = $formData;
            $tplVars['id_person'] = $id_person;
            $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name
                                            FROM person ORDER BY first_name");
            $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name"); //Variable deleted after iterating in template
            $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");
            return $this->view->render($response->withStatus(409), 'newRelation.latte', $tplVars);
        }
    }
    if (!empty($id_person)) {
        return $response->withRedirect($this->router->pathFor('personsInfo') . '?id_person=' . $id_person);
    }

    $relationId = $this->db->lastInsertId('relation_id_relation_seq');
    $tplVars['formData'] = $formData;
    return $response->withRedirect($this->router->pathFor('relationUpdate') . '?id_relation=' . $relationId);
});


$app->get('/relations/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $relationId = $params['id_relation'];
    if (empty($relationId)) {
        $tplVars['error'] = 'ID is missing';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    } else {
        $stmt = $this->db->prepare("SELECT * FROM relation 
                                    LEFT JOIN relation_type USING (id_relation_type)
                                    WHERE id_relation = :id_relation");
        $stmt->bindValue(":id_relation", $relationId);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
    }

    if (empty($tplVars['formData'])) {
        $tplVars['error'] = 'Relation not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }
    $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name"); //Variable deleted after iterating in template
    $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");

    return $this->view->render($response, 'relationUpdate.latte', $tplVars);
})->setName('relationUpdate');


$app->post('/relations/update', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $relationId = $request->getQueryParams()['id_relation'];
    if (empty($formData['id_person1']) or empty($formData['id_person2']) or empty($formData['id_person1'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        // Zapis relation
        try {
            $stmt = $this->db->prepare("UPDATE relation SET id_person1 = :id_person1, id_person2 = :id_person2, 
                                            description = :description, id_relation_type = :id_relation_type
                                            WHERE id_relation = :id_relation");
            $stmt->bindValue(":id_person1", $formData['id_person1']);
            $stmt->bindValue(":id_person2", $formData['id_person2']);
            $stmt->bindValue(":id_relation_type", $formData['id_relation_type']);
            $stmt->bindValue(":description", $formData['description']);
            $stmt->bindValue(":id_relation", empty($relationId) ? null : $relationId);
            $stmt->execute();
            $tplVars['message'] = 'Relation successfuly updated';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, something went wrong.';
            $response = $response->withStatus(409);
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['person1'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name");
    $tplVars['person2'] = $this->db->query("SELECT id_person, first_name, last_name 
                                            FROM person ORDER BY first_name"); //Variable deleted after iterating in template
    $tplVars['relation_type'] = $this->db->query("SELECT * FROM relation_type ORDER BY id_relation_type");
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'relationUpdate.latte', $tplVars);
});


$app->get('/relations/info', function (Request $request, Response $response, $args) {
    $relationId = $request->getQueryParams()['id_relation'];

    $stmt = $this->db->prepare("SELECT * FROM relation 
                                LEFT JOIN (SELECT id_person as id_person1, first_name as first_name1, last_name as last_name1 FROM person) as person1 ON relation.id_person1 = person1.id_person1
                                LEFT JOIN (SELECT id_person as id_person2, first_name as first_name2, last_name as last_name2 FROM person) as person2 ON relation.id_person2 = person2.id_person2
                                LEFT JOIN relation_type rt on relation.id_relation_type = rt.id_relation_type
                                WHERE id_relation = :id_relation
                                ORDER BY id_relation");
    $stmt->bindValue(":id_relation", empty($relationId) ? null : $relationId);
    $stmt->execute();
    $tplVars['relation'] = $stmt->fetch();

    if (empty($tplVars['relation'])) {
        $tplVars['error'] = 'Relation not found.';
        return $this->view->render($response->withStatus(404), 'pageNotFound.latte', $tplVars);
    }

    return $this->view->render($response, 'relationInfo.latte', $tplVars);
})->setName('relationInfo');


$app->post('/relations/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $relationId = $params['id_relation'];
    $personId = $params['id_person'];
    if (empty($relationId)) {
        $message = 'Invalid ID, relation could not be removed.';
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM relation WHERE id_relation = :id_relation");
            $stmt->bindValue(':id_relation', $relationId);
            $stmt->execute();
            $message = 'Relation successfuly removed.';

        } catch (PDOException $e) {
            $this->logger->info($e);
            $message = 'Sorry, something went wrong while deleting relation.';
        }
        if (!empty($personId)) {
            $tplVars['id_person'] = $personId;
            return $response->withRedirect($this->router->pathFor('personsInfo', [], $tplVars));
        }
    }

    $tplVars[base64_encode('message')] = base64_encode($message);
    return $response->withRedirect($this->router->pathFor('relations', [], $tplVars));
})->setName('relationDelete');