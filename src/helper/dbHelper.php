<?php


function insert_location($db, $formData) {
    try {
        $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip) VALUES 
                            (:city, :street_name, :street_number, :zip)");
        $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
        $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
        $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
        $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


function insert_contact($db, $formData, $person_id, $logger) {
    try {
        $stmt = $db->prepare("INSERT INTO contact (id_person, id_contact_type, contact) VALUES
                                                                 (:id_person, :contact_type, :contact)");
        $stmt->bindValue(":contact_type", empty($formData['contact_type']) ? null : $formData['contact_type']);
        $stmt->bindValue(":contact", empty($formData['contact']) ? null : $formData['contact']);
        $stmt->bindValue(":id_person", empty($person_id) ? null : $person_id);
        $stmt->execute();
    } catch (PDOException $e) {
        $formData['message'] = 'Contact save was unsuccessful';
        $logger->error($e->getMessage());
    }
}
